@extends('errors::minimal')
@section('title', __('Ocurrió un error'))
@section('code', '5050')
@section('message', __('Server Error'))
