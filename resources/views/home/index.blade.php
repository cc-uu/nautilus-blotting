@extends('layouts.app')
@section('title', ' | Catalogo')
@section('body-clase','landing-page sidebar-collapse')
@section('contenido')
@include('layouts.menu')

@foreach ($xParametros as $key => $val)
    @include('layouts.'.$val['tipo'])
@endforeach

@endsection
