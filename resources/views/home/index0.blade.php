@extends('layouts.app')
@section('title', ' | Catalogo')
@section('body-clase','landing-page sidebar-collapse')
@section('contenido')
    @include('layouts.menu')

    @if(!empty($xSlideHome))
      <div id="carouselExampleCaptions" class="carousel carousel-web slide-home slide" data-ride="carousel">
        <div class="container caption">
          <div class="col-md-10 offset-md-2 col-sm-8 offset-sm-1">
            <ol class="carousel-indicators">
              @foreach($xSlideHome as $key => $val)
                <li data-target="#carouselExampleCaptions" data-slide-to="{{ $key }}" class="{{ $key==0 ? 'active' : '' }}"></li>
              @endforeach
            </ol>
          </div>
        </div>
        <div class="carousel-inner">
          @foreach($xSlideHome as $key => $val)
            <div class="carousel-item {{ $key==0 ? 'active' : '' }}">
              <div class="centrar-img">
                <img src="{{ asset('images/home/slide-1.png') }}">
              </div>
              <div class="container caption">
                <div class="col-lg-5 offset-lg-2 col-md-8 offset-md-2 col-sm-7 offset-sm-1 col-9 p-0">
                  <h1>{{ $val->titulo }}</h1>
                  <p>{{ strip_tags($val->descripcion) }}</p>
                  <button class="btn btn-lg" onclick="{{ url('url', []) }}">BOTÓN</button>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="container caption">
          <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    @endif

    @if(!empty($xServicios))
      <section class="servicios">
        <div class="container">
          <div class="row">
            @foreach($xServicios as $key => $val)
              @if($val->portada == 'S')
                <div class="col-md-4 col-sm-12">
                  <div class="modulo-servicio">
                    <img src="{{ $val->imagenes[0]->path }}" alt="">
                    <h1>{{ $val->titulo }}</h1>
                    <p>{{ strip_tags($val->detalle) }}</p>
                  </div>
                </div>
              @endif
            @endforeach
          </div>
        </div>
      </section>
    @endif

    @if($xModuloPortada == false)
      @if(!empty($xNovedades))
        <section class="novedades">
          <div class="container">
            <div class="col-12">
              <h1 class="titulo-seccion">{{ $xMenu [3]['titulo'] }}</h1>
            </div>
            <div class="carousel carousel-responsive slide" id="carousel-novedades" data-ride="carousel" data-items="1,2,2,3,3">
              <div class="carousel-inner">
                @foreach($xNovedades as $key => $val)
                  @if($val->idContenido != config('parametros.idOcultarNovedad'))
                    <div class="carousel-item {{ $key==0 ? 'active' : '' }}">
                      <div class="modulo-novedad">
                        <div class="centrar-img">
                          <a href="{{url('seccion-cuatro/')}}/{{ Str::slug ($xMenu[3]['titulo']) }}/{{$val->idContenido}}/{{ Str::slug ($val->titulo) }}"><img src="{{ $val->imagenes[0]->path }}" alt="{{ $val->titulo }}"></a>
                        </div>
                        <h1>{{ $val->titulo }}</h1>
                        <p>{{ Str::words(strip_tags($val->descripcion), config('parametros.cantPalabrasNovedades'), config('parametros.finPalabrasNovedades')) }}</p>
                      </div>
                    </div>
                  @endif
                @endforeach
              </div>
              <a class="carousel-control-prev" href="#carousel-novedades" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel-novedades" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <button class="btn btn-lg btn-novedad"> Más novedades</button>
            </div>
          </div>
        </section>
      @endif
    @endif

    @if(!empty($xBanner))
      @php
      $back_img = null;
      if(!empty($xBanner->imagenes[0]->path)){
        $back_img = $xBanner->imagenes[0]->path;
      }
      @endphp
      <section class="call-to-action">
        <div class="container">
          <div class="col-xl-12 contenedor-action">
            <div class="info-call">
              @if(!empty($xBanner->bajada))
                <h1>{{ $xBanner->bajada }}</h1>
              @endif
              @if(!empty($xBanner->descripcion))
                <p>{{ strip_tags($xBanner->descripcion) }}</p>
              @endif
              @if($xBanner->link)
                <a href="{{ $xBanner->link }}">
                  <button class="btn btn-lg btn-call">{{ $xBanner->subtitulo }}</button>
                </a>
              @endif
            </div>
          </div>
        </div>
        <div class="centrar-img">
            <img src="{{ $back_img }}" alt="">
        </div>
      </section>
    @endif


    @if(!empty($xLogos))
        <section class="marcas">
          <div class="container">
            <h1 class="titulo-seccion">Confiaron en nosotros</h1>
            <div id="carousel-marcas" class="carousel carousel-responsive slide" data-ride="carousel" data-items="1,2,4,5,5">
              <div class="carousel-inner">
                @foreach($xLogos as $key => $val)
                  <div class="carousel-item {{ $key==0 ? 'active' : '' }}">
                    <div class="centrar-img">
                      @if(!empty($val->link))
                        <a target="_blank" href="{{ $val->link }}"><img src="{{ $val->imagenes[0]->path }}" alt=""></a>
                      @else
                        <img src="{{ $val->imagenes[0]->path }}" alt="">
                      @endif
                     </div>
                  </div>
                @endforeach
              </div>

              <a class="carousel-control-prev" href="#carousel-marcas" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
              </a>

              <a class="carousel-control-next" href="#carousel-marcas" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>

            </div>
          </div>
        </section>
    @endif

    @if($xModuloPortada == true)
      @if(!empty($xNovedades))
        <section class="cards">
          <div class="container">
            <div class="col-12">
              <h1 class="titulo-seccion"> Novedades</h1>
            </div>
            @if(!empty($xTags))
              <div class="row">
                <div class="col-lg-5 d-flex">
                  <p class="titulo-buscador">Elegir categoría</p>
                    <select name="basic" class="buscador" id="buscador-novedades">
                      <option value="" disabled selected>Seleccionar categoría</option>
                      @foreach($xTags as $key => $val)
                        <option value="{{ $val->nombre }}*{{ config('parametros.idNovedades') }}">{{ ucfirst($val->nombre) }}</option>
                      @endforeach
                    </select>
                </div>
              </div>
            @endif
            <div class="row" id="resultados-novedades">
              @foreach($xNovedades as $key => $val)
                @if($val->idContenido != config('parametros.idOcultarNovedad'))
                  <div class="col-md-6">
                    <div class="modulo-galeria">
                      <div class="centrar-img">
                        <img src="{{ $val->imagenes[0]->path }}" alt="{{ $val->titulo }}">
                      </div>
                      <h1>{{ $val->titulo }}</h1>
                      <p class="d-none d-md-block">{{ Str::words(strip_tags($val->descripcion), config('parametros.cantPalabrasNovedades'), config('parametros.finPalabrasNovedades')) }}</p>
                    </div>
                  </div>
                @endif
              @endforeach
            </div>
          </div>
        </section>
      @endif
    @endif

    @if(!is_null($xVideoHome->videos[0]->idVideo))
      {{-- Video de Vimeo --}}
    @endif
    @if(!is_null($xVideoHome->videos[1]->idVideo))
      <section class="video">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h1 class="titulo-seccion">{{ $xVideoHome->titulo }}</h1>
            </div>
            <div class="col-12">
              <iframe width="100%" height="auto" src="https://www.youtube.com/embed/{{ $xVideoHome->videos[1]->idVideo }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </section>
    @endif

@endsection
