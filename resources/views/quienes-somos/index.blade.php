@extends('layouts.app')
@section('title', ' | Catalogo')
@section('body-clase','landing-page sidebar-collapse')
@section('contenido')
@include('layouts.menu')

@if(!empty($xQuienesSomos))
    <section class="head-section">
        <div class="container">
            <div class="contenedor-head">
                <div class="info-head">
                    <h1>{{$xQuienesSomos->titulo}}</h1>
                    @if (!empty ($xQuienesSomos->descripcion))
                      <p>{!! html_entity_decode($xQuienesSomos->descripcion) !!}</p>
                    @endif
                </div>
            </div>
        </div>

    <div class="centrar-img">
        @if(!empty($xQuienesSomos->archivos[0]->path) && empty($xQuienesSomos->imagenes[0]->path))
            <video autoplay loop muted width="100%" height="100%">
                <source src="{{ $xQuienesSomos->archivos[0]->path }}" type="video/mp4">
            </video>
        @endif
        @if(empty($xQuienesSomos->archivos[0]->path) && !empty($xQuienesSomos->imagenes[0]->path))
            <img src="{{ $xQuienesSomos->imagenes[0]->path }}" alt="">
        @endif
        @if(!empty($xQuienesSomos->archivos[0]->path) && !empty($xQuienesSomos->imagenes[0]->path))
            <video autoplay loop muted width="100%" height="100%">
                <source src="{{ $xQuienesSomos->archivos[0]->path }}" type="video/mp4">
            </video>
        @endif
    </div>
</section>
@endif
@if(!empty($aModulo))
<section class="institucional">
    <div class="container">
            @foreach($aModulo as $key => $val)
                <div class="contenedor-institucional">
                    <div class="col-md-6 col-sm-12">
                            @if(count($val ['imagenes']) == 1)
                                @if($val ['imagenes'][0]->nombre != 'default_contenido')
                                    <div class="centrar-img">
                                        <img src="{{ $val['imagenes'][0]->path }}" alt="">
                                    </div>
                                @endif
                            @else
                                <div id="carousel-modulos-{{ $key }}" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        @foreach($val ['imagenes'] as $i_key => $i_val)
                                            <div class="carousel-item {{ $i_key==0 ? 'active' : '' }}">
                                                <div class="centrar-img">
                                                    <img src="{{ $i_val->path }}" alt="...">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel-modulos-{{ $key }}" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-modulos-{{ $key }}" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Siguiente</span>
                                    </a>
                                </div>
                            @endif
                            @if(!is_null($val ['videos'][0]->idVideo) && empty($val ['videos'][1]->idVideo))
                                <!-- {{-- Imprimimos el video de Vimeo --}} -->
                                <iframe src="{{$val ['videos'][0]->idVideo}}" uno width="100%" height="auto" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            @endif
                            @if(!empty($val ['videos'][1]->idVideo) && is_null($val ['videos'][0]->idVideo))
                                <!-- {{-- Imprimimos el video de Youtube --}} -->
                                <iframe width="100%" height="auto" dos src="{{$val ['videos'][1]->idVideo}}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            @endif
                            @if (!empty($val ['videos'][1]->idVideo) && !is_null($val ['videos'][0]->idVideo))
                                <!-- {{-- Imprimimos el video de Youtube --}} -->
                              <iframe width="100%" height="auto" tres src="https://www.youtube.com/embed/{{ $val ['videos'][1]->idVideo}}?&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            @endif
                            <!-- @if(!is_null($val ['videos'][0]->idVideo) && is_null($val ['videos'][0]->idVideo)) -->
                            <!-- <img src="{{ $val->imagenes[0]->path }}" alt=""> -->
                            <!-- @endif -->
                        </div>

                    <div class="col-md-6 col-sm-12 contenedor-info">
                        <h1>{{ $val ['titulo'] }}</h1>
                        @if(!empty($val ['descripcion']))
                            <p>{!! html_entity_decode($val ['descripcion']) !!}</p>
                        @endif
                    </div>
                </div>
            @endforeach
    </div>
</section>
@endif
@if(!empty($aVision))
<section class="valores">
    <div class="container">
        <div class="row">
            @foreach($aVision as $key => $val)
            <div class="col-md-4 col-sm-12">
                <h1>{{$val ['titulo']}}</h1>
                <p>{!! html_entity_decode($val ['descripcion']) !!}
                </p>
            </div>
        @endforeach
    </div>
    </div>
</section>
@endif

@if(!empty($aGaleria))
    <section class="galeria">
        <div class="container">
          @foreach($aGaleria as $k_key => $k_val)
            <h1 class="titulo-seccion">{{ $k_val['titulo'] }}</h1>
            @endforeach
            <div class="row">
                      @foreach($aGaleria[0]['imagenes'] as $g_key => $g_val)
                      <div class="col-sm-6 mt-4">
                          <div class="centrar-img">
                          <img src="{{ $g_val->path }}" alt="">
                          </div>
                          </div>
                      @endforeach
            </div>
        </div>
    </section>
@endif

@if(!empty($xQuienesSomos))
    <section class="video">
        <div class="container">
        <div class="row">
            <div class="col-12">
            <h1 class="titulo-seccion">{{ $xQuienesSomos->subtitulo }}</h1>
            </div>
            <div class="col-12">
            @foreach ($aModulo as $ñ_key => $ñ_val)
             @if(!empty($ñ_val ['videos'][1]->idVideo))
               <iframe width="100%" height="auto" src="https://www.youtube.com/embed/{{ $ñ_val ['videos'][1]->idVideo}}?&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              @endif
             @endforeach
            </div>
        </div>
        </div>
    </section>
@endif
@endsection
