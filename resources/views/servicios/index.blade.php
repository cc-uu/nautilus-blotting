@extends('layouts.app')
@section('title', ' | Catalogo')
@section('body-clase','landing-page sidebar-collapse')
@section('contenido')
@include('layouts.menu')

@if(!empty($xIntroServi))
<section class="head-section">
    <div class="container">
        <div class="contenedor-head">
            <div class="info-head">
                <h1>{{ $xIntroServi->titulo }}</h1>
                @if (!empty ($xIntroServi->descripcion))
                    <p>{!! html_entity_decode($xIntroServi->descripcion) !!}</p>
                @endif
            </div>
        </div>
    </div>

    <div class="centrar-img">
        @if(!empty($xIntroServi->archivos[0]->path) && empty($xIntroServi->imagenes[0]->path))
            <video autoplay loop muted width="100%" height="100%">
                <source src="{{ $xIntroServi->archivos[0]->path }}" type="video/mp4">
            </video>
        @endif
        @if(empty($xIntroServi->archivos[0]->path) && !empty($xIntroServi->imagenes[0]->path))
            <img src="{{ $xIntroServi->imagenes[0]->path }}" alt="">
        @endif
        @if(!empty($xIntroServi->archivos[0]->path) && !empty($xIntroServi->imagenes[0]->path))
            <video autoplay loop muted width="100%" height="100%">
                <source src="{{ $xIntroServi->archivos[0]->path }}" type="video/mp4">
            </video>
        @endif
    </div>
</section>
@endif

@if(!empty($xTags))
<section class="servicios">
    <div class="container">
          <div class="row">
            <div class="col-lg-5 d-flex">
              <p class="titulo-buscador">Elegir {{$xMenu [2]['titulo'] }}</p>
              <div class="cont-buscador">
                <select name="basic" class="buscador" id="buscador-servicios">
                    @foreach($xTags as $key => $val)
                    <option {{ $key = 0 ? 'selected' : '' }} value="{{ $val->nombre }}*{{ config('parametros.idServicios') }}">{{ ucfirst($val->nombre) }}</option>
                    @endforeach
                </select>
              </div>
            </div>
          </div>
          <div id="resultados-servicios"></div>
    </div>
</section>
@endif

@if(!empty($aVideos))
    <section class="carousel-video">
        <h1 class="titulo-seccion text-center">{{$xIntroServi->bajada}}</h1>
            <div class="carousel slide" data-ride="carousel" id="carousel-video">
                <div class="container position-relative">
                    @if(count($aVideos) > 1)
                        <ol class="carousel-indicators">
                             @foreach($aVideos as $key => $val)
                                <li data-target="#carousel-video" data-slide-to="{{ $key }}" class="{{ $key==0 ? 'active' : '' }}"></li>
                             @endforeach
                        </ol>
                    @endif
                    <div class="carousel-inner">
                        @foreach($aVideos as $key => $val)
                        <div class="carousel-item {{ $key==0 ? 'active' : '' }}">
                            <div class="col-lg-10 offset-lg-1 px-md-0">
                                <iframe width="100%" height="auto" src="https://www.youtube.com/embed/{{$val['videos'][1]->idVideo}}?&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    @if(count($aVideos) > 1)
                        <a class="carousel-control-prev" href="#carousel-video" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon svg" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-video" role="button" data-slide="next">
                          <span class="carousel-control-next-icon svg" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                    @endif
                </div>
            </div>
            </section>
            @endif

            @if(!empty($aVision))
            <section class="valores">
                <div class="container">
                    <div class="row">
                        @foreach($aVision as $key => $val)
                        <div class="col-md-4 col-sm-12">
                            <h1>{{$val ['titulo']}}</h1>
                            <p>{!! html_entity_decode($val ['descripcion']) !!}
                            </p>
                        </div>
                    @endforeach
                </div>
                </div>
            </section>
            @endif

@if(!empty($aBanner))
<section class="call-to-action">
    <div class="container">
        @foreach ($aBanner as $key => $val)
      <div class="col-xl-12 contenedor-action">
        <div class="info-call">
          <h1>{{$val ['bajada']}}</h1>
          <p>{!! html_entity_decode($val ['descripcion']) !!}</p>
          <a href="{{ $val ['link'] }}" target="_blank">
            <button class="btn btn-lg btn-call"> {{$val ['subtitulo']}}</button>
          </a>
        </div>
      </div>
      @endforeach
    </div>
    @if($xImgBanner)
    <div class="centrar-img">
      <img src="{{ $xImgBanner->imagenes[0]->path }}" alt="">
    </div>
    @endif
</section>
@endif
@endsection
