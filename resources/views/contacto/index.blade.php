@extends('layouts.app')
@section('title', ' | Catalogo')
@section('body-clase','landing-page sidebar-collapse')
@section('contenido')
@include('layouts.menu')

  @if(!empty($xContacto))
    @if($xContacto->detalle != "" || ($xContacto->imagenes[0]->nombre == ""))
    <section class="head-section">
      <div class="centrar-img">
       @if($xContacto->detalle != "" && ($xContacto->imagenes[0]->nombre == ""))
         <img src="{{$xContacto->imagenes[0]->path}}" alt="">
       @endif
       @if($xContacto->detalle != "" && ($xContacto->imagenes[0]->nombre != ""))
           {!! str_replace(["<p>", "</p>"], "", $xContacto->detalle) !!}
       @endif
       @if($xContacto->detalle == "" && ($xContacto->imagenes[0]->nombre == ""))
         <img src="{{$xContacto->imagenes[0]->path}}" alt="">
       @endif
      </div>
    </section>
    @endif
    <section class="contacto">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h1 class="titulo-seccion">{{ $xContacto->titulo }}</h1>
         </div>
        </div>
        <div class="row">
          <div class="col-lg-5 col-md-6 col-sm-12 formulario">
            <!--<p>Formulario</p>-->
             <div class="container-responsive">
               @if(!empty($xContacto->link))
                <iframe width="100%" height="510" frameborder="0" class="container-responsive" src="{{$xContacto->link}}" allowfullscreen></iframe>
               @endif
               <p style="margin-left: 290px;">(*) Required data</p>
             </div>
          </div>
            <div class="col-xl-5 offset-xl-1 col-lg-6 offset-lg-1 col-md-6 col-sm-12 info-formulario">
                @if(!empty($xContacto))
                  <p>{!! html_entity_decode($xContacto->descripcion) !!}</p>
                @endif
            </div>
        </div>
        </div>
     </section>
     @else
     <section class="contacto">
        <div class="container">
          <h2 class="titulo-seccion">En construcción </h2>
        </div>
     </section>
     @endif
@endsection
