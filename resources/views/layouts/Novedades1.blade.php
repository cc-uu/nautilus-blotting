@if(!empty($val['contenido']))
<section class="novedades">
  <div class="container">
    <h1 class="titulo-seccion">{{ ($xMenu[3]['titulo']) }}</h1>
    <div class="carousel carousel-responsive slide" id="carousel-novedades" data-ride="carousel" data-items="1,2,2,3,3">
      <div class="carousel-inner">
        @php $cantidad = 0 @endphp
        @foreach($val['contenido'] as $n_key => $n_val)
          @if($n_val->portada)
            @php $cantidad = $cantidad + 1 @endphp
            <div class="carousel-item {{ $n_key==0 ? 'active' : '' }}">
              <div class="modulo-novedad">
                <div class="centrar-img">
                  <a href="{{ Str::slug ($xMenu[3]['titulo']) }}/{{$n_val->idContenido}}/{{ Str::slug ($n_val->titulo) }}"><img src="{{ $n_val->imagenes[0]->path }}" alt="{{ $n_val->titulo }}"><h1>{{ $n_val->titulo }}</h1></a>
                </div>
                    <a href="{{ Str::slug ($xMenu[3]['titulo']) }}/{{$n_val->idContenido}}/{{ Str::slug ($n_val->titulo) }}"><h1>{{ $n_val->titulo }}</h1></a>
                <p>{{ Str::words(html_entity_decode(strip_tags($n_val->descripcion)), config('parametros.cantPalabrasNovedades'), config('parametros.finPalabrasNovedades')) }}</p>
              </div>
            </div>
          @endif
        @endforeach
      </div>
      @if($cantidad > 3)
        <a class="carousel-control-prev" href="#carousel-novedades" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Anterior</span>
        </a>
        <a class="carousel-control-next" href="#carousel-novedades" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Siguiente</span>
        </a>
      @endif
      <a href="{{ Str::slug ($xMenu[3]['titulo']) }}"><button class="btn btn-lg btn-novedad">More</button></a>
    </div>
  </div>
</section>
@endif
