@if(!empty($val['contenido']))
<section class="servicios">
  <div class="container">
    <div class="row">
      @foreach($val['contenido'] as $t_key => $t_val)
        @if($t_val->portada == 'S')
          <div class="col-md-4 col-sm-12">
            <div class="modulo-servicio">
              <!-- si tiene link
              <a href="">-->
                  @if(!empty($t_val->archivos[0]->path) && empty($t_val->imagenes[0]->path))
                      <video autoplay loop muted width="100%" height="100%">
                          <source src="{{ $t_val->archivos[0]->path }}" type="video/mp4">
                      </video>
                  @endif
                  @if(empty($t_val->archivos[0]->path) && !empty($t_val->imagenes[0]->path))
                      <img src="{{ $t_val->imagenes[0]->path }}" alt="" class="img-fluid">
                  @endif
                  @if(!empty($t_val->archivos[0]->path) && !empty($t_val->imagenes[0]->path))
                      <video autoplay loop muted width="100%" height="100%">
                          <source src="{{ $t_val->archivos[0]->path }}" type="video/mp4">
                      </video>
                  @endif

                  <h1>{{ $t_val->titulo }}</h1>
                @if(!empty($t_val->link == ""))
                 <p>{!! $t_val->descripcion !!}</p>
                 @endif
                 @if(!empty($t_val->link))
                 <a href="{{ ($t_val->link)}}"><p>{!! $t_val->descripcion !!}</p></a>
                 @endif
              <!-- si tiene link
              </a>-->
            </div>
          </div>
        @endif
      @endforeach
    </div>
  </div>
</section>
@endif
