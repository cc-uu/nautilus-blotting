@if(!empty($val['contenido']))
    <div id="slideHome" class="carousel carousel-web slide-home slide" data-ride="carousel">
     @if(count($val['contenido']) > 1)
      <div class="container caption">
        <div class="col-md-10 offset-md-2 col-sm-8 offset-sm-1">
          <ol class="carousel-indicators">
            @foreach($val['contenido'] as $s_key => $s_val)
              <li data-target="#slideHome" data-slide-to="{{ $s_key }}" class="{{ $s_key==0 ? 'active' : '' }}"></li>
            @endforeach
          </ol>
        </div>
      </div>
      @endif
      <div class="carousel-inner">
        @foreach($val['contenido'] as $s_key => $s_val)
          <div class="carousel-item {{ $s_key==0 ? 'active' : '' }}">
            <div class="centrar-img">
                @if(!empty($s_val->archivos[0]->path) && empty($s_val->imagenes[0]->path))
                    <video autoplay loop muted width="100%" height="100%">
                        <source src="{{ $s_val->archivos[0]->path }}" type="video/mp4">
                    </video>
                @endif
                @if(empty($s_val->archivos[0]->path) && !empty($s_val->imagenes[0]->path))
                    <img src="{{ $s_val->imagenes[0]->path }}" alt="">
                @endif
                @if(!empty($s_val->archivos[0]->path) && !empty($s_val->imagenes[0]->path))
                    <video autoplay loop muted width="100%" height="100%">
                        <source src="{{ $s_val->archivos[0]->path }}" type="video/mp4">
                    </video>
                @endif
            </div>
            <div class="container caption">
              <div class="col-xl-4 col-lg-5 offset-lg-2 col-md-6 offset-md-2 col-sm-7 offset-sm-1 col-9 p-0">
                <h1>{{ $s_val->titulo }}</h1>
                <p>{{ html_entity_decode(strip_tags($s_val->descripcion)) }}</p>
                @if(!empty($s_val->link))
                  <a href="{{ $s_val->link}}" target="_blank"><button type="button" class="btn btn-lg">{{ $s_val->subtitulo }}</button></a>
                @endif
              </div>
            </div>
          </div>
        @endforeach
      </div>
      @if(count($val['contenido']) > 1)
      <div class="container">
        <a class="carousel-control-prev" href="#slideHome" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon svg" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#slideHome" role="button" data-slide="next">
            <span class="carousel-control-next-icon svg" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
      </div>
      @endif
    </div>
@endif
