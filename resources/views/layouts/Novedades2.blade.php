@if(!empty($val['contenido']))
<section class="cards">
  <div class="container">
    <h1 class="titulo-seccion">{{ $xMenu [3]['titulo'] }}</h1>
    @if(!empty($val['tags']))
      <div class="row">
        <div class="col-lg-6 d-flex">
          <p class="titulo-buscador">Elegir categoría</p>
            <select name="basic" class="buscador" id="buscador-novedades">
              <option value="" disabled selected>Seleccionar categoría</option>
              @foreach($val['tags'] as $t_key => $t_val)
                <option value="{{ $t_val->nombre }}*{{ config('parametros.idNovedades') }}">{{ ucfirst($t_val->nombre) }}</option>
              @endforeach
                <option value="todas">Ver todas</option>
            </select>
        </div>
      </div>
    @endif
    <div class="row" id="resultados-novedades">
      @foreach($val['contenido'] as $t_key => $t_val)
          <div class="col-md-6">
            <div class="modulo-galeria">
              <div class="centrar-img">
                 <a href="{{ Str::slug ($xMenu[3]['titulo']) }}/{{$t_val->idContenido}}/{{ Str::slug ($t_val->titulo) }}">
                   <img src="{{ $t_val->imagenes[0]->path }}" alt="{{ $t_val->titulo }}">
                 </a>
              </div>
              <a href="{{ Str::slug ($xMenu[3]['titulo']) }}/{{$t_val->idContenido}}/{{ Str::slug ($t_val->titulo) }}">
                <h1>{{ $t_val->titulo }}</h1>
              </a>
              <p class="d-none d-md-block">{{ Str::words(html_entity_decode(strip_tags($t_val->descripcion)), config('parametros.cantPalabrasNovedades'), config('parametros.finPalabrasNovedades')) }}</p>
            </div>
          </div>
      @endforeach
    </div>
  </div>
</section>
@endif
