<footer>
    <div class="container">
        <div class="row">
            <!--<div class="col-lg-5">
            </div>
            <div class="col-lg-2 offset-lg-1 col-md-4 offset-md-0">-->
            <div class="col-lg-2 col-md-4">
                <ul class="nav-footer">
                @foreach($xMenu as $key => $val)
                   <li>
                     <a href="{{URL::to($val['enlace'])}}">{{ $val['titulo'] }}</a>
                   </li>
                @endforeach
                </ul>
            </div>
            

            @if(!empty($xFooter))
              <div class="col-lg-3 offset-lg-1 col-md-4 offset-md-0 contact-footer">
                  <h3>{{$xFooter->titulo}}</h3>
                  <p>{!! html_entity_decode($xFooter->descripcion) !!}</p>                  
                  @if(!empty($aSociales))
                  @foreach($aSociales as $key => $val)
                  <a target="_blank" class="redes" href="{{$val['bajada']}}">
                      <img src="{{ $val['imagenes'][0]->path }}" alt="">
                  </a>                  
                  @endforeach
                  @endif
              </div>
            @endif
        </div>
        <div class="container pie-agencia">  
            <ul>
                <li>
                    <a href="http://nautilus.la/" target="_blank">
                        <img src="https://app.fidelitytools.net/img/pie-agencia/nautilus.svg">
                    </a>
                </li>
                <li>
                    <a href="http://www.sawubona.com.ar/" target="_blank">
                        <img src="https://app.fidelitytools.net/img/pie-agencia/sawubona.svg">
                    </a>
                </li>
                <li>
                    <a href="http://www.fidelitytools.com/" target="_blank">
                        <img src="https://app.fidelitytools.net/img/pie-agencia/fidelitytools.svg">
                    </a>
                </li>
                <li>
                    <a href="http://www.tomnit.com/" target="_blank">
                        <img src="https://app.fidelitytools.net/img/pie-agencia/tomnit.svg">
                    </a>
                </li>
                <li>
                    <a href="http://www.alojanet.com/" target="_blank">
                        <img src="https://app.fidelitytools.net/img/pie-agencia/alojanet.svg">
                    </a>
                </li>
            </ul>
        </div>  
    </div>
</footer>
