
<nav class="navbar navbar-expand-lg" id="nav">
    <div class="container">
        <!-- Logo -->
        <a class="navbar-brand" href="{{url('/')}}">
            <img class="icon-r" src="{{ $xLogo->imagenes[0]->path }}" alt="">
        </a>

        <div class="d-flex">
            <div class="dropdown menu-idiomas show">
                <!-- <a class="btn dropdown-toggle" href="#" role="button" id="dropdownIdioma" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ENG
                </a> -->

                <!-- <div class="dropdown-menu" aria-labelledby="dropdownIdioma">
                    <a class="dropdown-item" href="http://sitios.fidelitytools.com/innovos-nautilus/">ESP</a>
                </div> -->
            </div>

            <!-- Button mobile -->
            <button class="navbar-toggler" type="button">
                <div class="hamburguesa">
                    <div id="linea1" class="linea"></div>
                    <div id="linea2" class="linea"></div>
                    <div id="linea3" class="linea"></div>
                </div>
            </button>
            <!-- Navbar links -->
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="navbar-nav">
                @foreach($xMenu as $key => $val)
                @if($key == 0)
                    <li class="nav-item d-flex align-items-center">
                        <a class="nav-link {{ request()->is($val['enlace']) ? 'active' : ''}}" href="{{URL::to($val['enlace'])}}">{{ $val['titulo'] }}</a>
                    </li>
                @else
                <li class="nav-item d-flex align-items-center">
                    <a class="nav-link {{ Request::path() == str_replace('/', '', $val['enlace']) ? 'active' : ''}}" href="{{URL::to($val['enlace'])}}">{{ $val['titulo'] }}</a>
                </li>
                @endif
                @endforeach
                </ul>
            </div>
        </div>

    </div>

    <div id="menu-md">
        <div class="container">
            <ul class="navbar-nav">
              @foreach($xMenu as $key => $val)
               @if($key == 0)
               <li class="nav-item d-flex align-items-center">
                   <a class="nav-link {{ request()->is($val['enlace']) ? 'active' : ''}}" href="{{URL::to($val['enlace'])}}">{{ $val['titulo'] }}</a>
               </li>
               @else
                  <li class="nav-item d-flex align-items-center">
                    <a class="nav-link {{ Request::path() == str_replace('/', '', $val['enlace']) ? 'active' : ''}}" href="{{URL::to($val['enlace'])}}">{{ $val['titulo'] }}</a>
                  </li>
              @endif
              @endforeach
            </ul>
        </div>
    </div>
</nav>
