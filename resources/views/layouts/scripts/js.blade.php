<script>
    var Sawubona = {
        baseUrl: baseUrl,
        map: null,
        onloadImage: null,
        scrolling: null,
        shopping: null,
        usuarioTwitter: null
    }
</script>
<script type="text/javascript" src="{{asset('/js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('/libs/jquery/jquery.touch/jquery.touch.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/libs/bootstrap/bootstrap.carousel-responsive/bootstrap.carousel-responsive.js')}}"></script>
<script type="text/javascript" src="{{asset('/libs/sawubona/sawubona.footer/sawubona.footer.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/appmin.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/core/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/shared.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/picker.min.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/numeric.js')}}"></script>
<script src="https://omnicanalapi.tech/chat/js/omnicanalLib.js"></script>

<script type="text/javascript">
      var token = "eyJ0aXR1bG8iOiJCaWVudmVuaWRvIGVuIHF1ZSBwb2RlbW9zIGF5dWRhcmxvPyIsImltZ0VtcHJlc2EiOiJodHRwczovL2ltYWdlbmVzLmZpZGVsaXR5dG9vbHMubmV0L2ltZy9Nak0xLzE5NTE5NSIsImltZ1VzdWFyaW8iOiJodHRwczovL2ltYWdlbmVzLmZpZGVsaXR5dG9vbHMubmV0L2ltZy9Nak0xLzE5NTE5NiIsInNlc3Npb25UaW1lb3V0IjoiNjA0ODAwIiwiY29sRGlhbG9nIjoiI2ZiZjZmMyIsImNvbEVtcHJlc2EiOiIjNGE0YTQ3IiwiY29sVXNlciI6IiMwMDMzODMiLCJjb2xJbnBUeHQiOiIjZmZmZmZmIiwiY29sSW5wQmciOiIjYjdhMzhmIiwiY29sSW5wRm9jdXMiOiIjZTdkYmNmIiwiY29sQmFjazEiOiIjZGFjOWI4IiwiY29sQmFjazIiOiIjZGFjOWI4IiwiY29sQmFjazMiOiIjZGFjOWI4IiwiY29sQmFjazQiOiIjZGFjOWI4IiwiY29sQmFjazUiOiIjZGFjOWI4IiwicGFnZUlkIjoiaWFHWjVSNmFTN1RNdk1wSWRDVmJBbmYySDdrZ2tLIn0";
      window.api = new OmniCanalApi(token);

      // la proxima linea iniciara el webchat desplegado
      //api.openElement();

      // la proxima linea cambia una regla del CSS del chat. En este caso la distancia a la base de la pagina
      // $('.floating-chat').css('bottom', '40px');
</script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', '<?= config("main.ID_ANALYTIICS") ?>', 'auto');
ga('send', 'pageview');
</script>
