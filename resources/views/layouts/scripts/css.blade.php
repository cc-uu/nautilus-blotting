<?php
$oContenido = new \App\Clases\Contenido\Contenido();
$xIcono = $oContenido->getById(config('parametros.idIcono'), config('parametros.idIconoCont'));
?>

<link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('/css/bootstrap-grid.css') }}">
<link rel="stylesheet" href="{{ asset('/css/bootstrap-reboot.css') }}">
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel="stylesheet" href="{{ asset('/css/app.css') }}">
<link rel="stylesheet" href="{{ asset('/css/picker.min.css') }}">
<link rel="stylesheet" href="{{ asset('/css/icons-font.css') }}">
<link rel="shortcut icon" href="{{ $xIcono->imagenes[0]->path }}">
