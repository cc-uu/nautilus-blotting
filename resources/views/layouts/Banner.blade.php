@if(!empty($val['contenido']))
@foreach($val['contenido'] as $b_key => $b_val)
    <section class="call-to-action">
      <div class="container">
        <div class="col-xl-12 contenedor-action">
          <div class="info-call">
            @if(!empty($b_val->bajada))
              <h1>{{ $b_val->bajada }}</h1>
            @endif
            @if(!empty($b_val->descripcion))
              <p>{{  html_entity_decode(strip_tags($b_val->descripcion)) }}</p>
            @endif
            @if($b_val->link)
              <a href="{{ $b_val->link }}" target="_blank">
                <button class="btn btn-lg btn-call">{{ $b_val->subtitulo }}</button>
              </a>
            @endif
          </div>
        </div>
      </div>
      <div class="centrar-img">
          <img src="{{ $b_val->imagenes[0]->path }}" alt="">
      </div>
    </section>
@endforeach
@endif
