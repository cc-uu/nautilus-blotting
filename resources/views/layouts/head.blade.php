<?php
$oContenido = new \App\Clases\Contenido\Contenido();
$xMeta = $oContenido->getById(config('parametros.idMeta'), config('parametros.idMetaCont'));
$zHome = $oContenido->getById(config('parametros.idNConfiguracion'), config('parametros.idNConfiguracionTitCont'));


?>

<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="csrf-token" content="{{ csrf_token() }}" />
<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
<meta name="description" content="{{strip_tags($xMeta->descripcion)}}"/>
<meta name="keywords" content="{{strip_tags($xMeta->detalle)}}" />
<meta name="viewport" content="width=device-width, initial-scale=1">


  <title>{{ explode('*', $zHome->precio)[0] }}  {{ str_replace('/', '', ucfirst(Request::path())) != '' ? '- '.str_replace('/', '', ucfirst(Request::path())) : '' }}</title>


<script type="text/javascript">
	var baseUrl = "<?php echo URL::to('/'); ?>/";
</script>
