@if(!empty($val['contenido']))

<section class="marcas">
  <div class="container">
      @if($xLogostitulos->titulo)
    <h1 class="titulo-seccion">{{ $xLogostitulos->titulo }}</h1>
     @endif
    <div id="carousel-marcas" class="carousel carousel-responsive slide" data-ride="carousel">
      <div class="carousel-inner">
        @foreach($val['contenido'] as $l_key => $l_val)
        @if($l_val->imagenes[0]->nombre != "default_contenido") 
          <div class="carousel-item {{ $l_key==0 ? 'active' : '' }}">
            <div class="centrar-img">
              @if(!empty($l_val->link))
                <a target="_blank" href="{{ $l_val->link }}"><img src="{{ $l_val->imagenes[0]->path }}" alt=""></a>
              @else
                <img src="{{ $l_val->imagenes[0]->path }}" alt="">
              @endif
             </div>
          </div>
          @endif
        @endforeach
      </div>
  @if(count($val['contenido']) > 5)
      <a class="carousel-control-prev" href="#carousel-marcas" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Anterior</span>
      </a>
      <a class="carousel-control-next" href="#carousel-marcas" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Siguiente</span>
      </a>
 @endif
    </div>
 </div>
</section>
@endif
