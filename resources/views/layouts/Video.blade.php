<br>
<br>
<br>
@if(!empty($val['contenido']))
    <section class="video">
            <div class="carousel slide" data-ride="carousel" id="carousel-video">
                <div class="container position-relative">
                  @if(count($val['contenido']) > 1)
                      <ol class="carousel-indicators">
                        @foreach($val['contenido'] as $a_key => $a_val)
                        <li data-target="#carousel-video" data-slide-to="{{ $a_key }}" class="{{ $a_key==0 ? 'active' : '' }}"></li>
                        @endforeach
                      </ol>
                      @endif
                    <div class="carousel-inner">
                        @foreach($val['contenido'] as $t_key => $t_val)
                        <div class="carousel-item {{ $t_key==0 ? 'active' : '' }}">
                          <div class="container">
                            <h3 class="titulo-seccion">{{ $t_val->titulo }}</h3>
                          </div>
                            <div class="container">
                              <iframe width="100%" height="auto" src="https://www.youtube.com/embed/{{ $t_val->videos[1]->idVideo }}?&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            </section>
@endif
