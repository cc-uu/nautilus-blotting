@extends('layouts.app')
@section('title', ' | Catalogo')
@section('body-clase','landing-page sidebar-collapse')
@section('contenido')
@include('layouts.menu')

@if(!empty($xIntroNovedades))
    <section class="head-section">
        <div class="container">
            <div class="contenedor-head">
                <div class="info-head">
                    <h1>{{ $xIntroNovedades->titulo }}</h1>
                    @if(!empty($xIntroNovedades->descripcion))
                        <p>{!! html_entity_decode($xIntroNovedades->descripcion) !!}</p>
                    @endif
                </div>
            </div>
        </div>

        <div class="centrar-img">
            @if(!empty($xIntroNovedades->archivos[0]->path) && empty($xIntroNovedades->imagenes[0]->path))
                <video autoplay loop muted width="100%" height="100%">
                    <source src="{{ $xIntroNovedades->archivos[0]->path }}" type="video/mp4">
                </video>
            @endif
            @if(empty($xIntroNovedades->archivos[0]->path) && !empty($xIntroNovedades->imagenes[0]->path))
                <img src="{{ $xIntroNovedades->imagenes[0]->path }}" alt="">
            @endif
            @if(!empty($xIntroNovedades->archivos[0]->path) && !empty($xIntroNovedades->imagenes[0]->path))
                <video autoplay loop muted width="100%" height="100%">
                    <source src="{{ $xIntroNovedades->archivos[0]->path }}" type="video/mp4">
                </video>
            @endif
        </div>
    </section>
    @endif

@if(!empty($xNovedades))
<section class="novedades">
    <div class="container">

          <h1 class="titulo-seccion">{{ $xIntroNovedades->bajada }}</h1>

      <div class="row">
        @foreach($xNovedades as $key => $val)
        <div class="col-lg-4 col-md-6 mt-4 mb-4">
          <div class="modulo-novedad">
            <a href="{{ Str::slug ($xMenu[3]['titulo']) }}/{{$val->idContenido}}/{{ Str::slug ($val->titulo) }}">
              <div class="centrar-img">
              @if (!empty ($val->imagenes[0]->path))
              <img src="{{ $val->imagenes[0]->path }}" alt="">
              @endif
              </div>
              @if (!empty ($val->titulo))
              <h1>{{$val->titulo}}</h1>
              @endif
              @if (!empty ($val->descripcion))
              <p>{{ Str::words(html_entity_decode(strip_tags($val->descripcion)), config('parametros.cantPalabrasNovedades'), config('parametros.finPalabrasNovedades')) }}</p>
              @endif
            </a>
          </div>
        </div>
        @endforeach

      </div>

@if (count($xTotalNovedades) > config('parametros.porPaginaNovedades'))
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">

          <li class="page-item arrows {{ $xPagina <= 1 ? 'disabled' : '' }}">
          <a class="page-link" href="{{ $xPagina <= 1 ? '#' : '?pagina='.($xPagina - 1) }}" tabindex="-1">
            <span class="icon-prev"></span>
          </a>
          </li>
          @for($i=1; $i<=$xTotalPaginas; $i++)
            <li class="page-item {{$i==$xPagina ? 'active' : ''}}"><a class="page-link" href="?pagina={{ $i }}">{{ $i }}</a></li>
          @endfor
          <li class="page-item arrows {{ $xPagina >= $xTotalPaginas ? 'disabled' : '' }}">
          <a class="page-link" href="{{ $xPagina >= $xTotalPaginas ? '#' : '?pagina='.($xPagina + 1) }}">
            <span class="icon-next"></span>
          </a>
          </li>

        </ul>
      </nav>
@endif
    </div>
  </section>
  @else
  <section class="novedades">
      <div class="container">
          <h1 class="titulo-seccion">No se encontraron {{ $xMenu [3]['titulo'] }}</h1>
      </div>
  </div>
  @endif


@endsection
