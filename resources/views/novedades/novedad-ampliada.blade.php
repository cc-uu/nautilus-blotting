@extends('layouts.app')
@section('title', ' | Catalogo')
@section('body-clase','landing-page sidebar-collapse')
@section('contenido')
@include('layouts.menu')

    <section class="nav-novedad">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-1 col-sm-1 col-2">
                <a href="{{url()->previous()}}" class="volver">
                    <span class="icon-prev"></span>
                </a>
                </div>
                <div class="col-lg-5 col-sm-6 col-10">
                    <p class="callback"><a href="{{url('/comunidad')}}">{{ $xMenu[3]['titulo'] }}</a> / <a style="pointer-events: none">{{$xNovedad->titulo}}</a></p>
                </div>
                <div class="col-xl-3 col-md-4 col-sm-5 d-none d-sm-block">
                    <p class="compartir">Compartir
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current()}}">  
                            <span class="icon-facebook"></span>
                        </a>
                        <a target="_blank" href="https://twitter.com/?status=Me gusta esta página {{ url()->current()}}">
                            <span class="icon-twitter"></span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>

@if(!empty($xNovedad))
    <section class="ampliada">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 col-lg-9 offset-lg-2 col-md-10 offset-md-1 col-sm-12">
                    <div class="contenedor-novedad">
                      @if(count($xNovedad->imagenes) > 1)
                        <!-- Si tiene más de una imagen -->
                        <div id="carouselNovedad" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                              @foreach($xNovedad->imagenes as $key => $val)
                                <div class="carousel-item {{ $key==0 ? 'active' : '' }}">
                                  <img class="d-block w-100" src="{{$val->path}}">
                                </div>
                              @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselNovedad" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselNovedad" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                      @else
                        <div class="centrar-img">
                            <img src="{{ $xNovedad->imagenes[0]->path }}" alt="">
                        </div>
                      @endif
                        <div class="col-md-4 col-sm-5 d-block d-sm-none p-0">
                            <p class="compartir">Compartir
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current()}}">
                                    <span class="icon-facebook"></span>
                                </a>
                                <a href="https://twitter.com/?status=Me gusta esta página {{ url()->current()}}">
                                    <span class="icon-twitter"></span>
                                </a>
                            </p>
                        </div>
                        <h1>{{$xNovedad->titulo}}</h1>
                        <p>{!! html_entity_decode($xNovedad->descripcion) !!}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    @endsection
