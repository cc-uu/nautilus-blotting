/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});








//A. Construir un algoritmo que permita ingresar 5 notas de exámenes y muestre el promedio de dichas notas.

const notas = [];

while (notas.length < 5) {
    nota = prompt('Ingresa nota: ');
    notas.push(nota);
}

const notasNum = notas.map(Number);
const totalNotas = notasNum.reduce((previous, current) => current += previous);
const promedio = totalNotas / notas.length;
console.log(promedio);


//B. Construir un algoritmo que permita obtener el porcentaje de hombres y mujeres que tiene un profesor en clase. Mostrar el total de alumnos y los porcentajes obtenidos.

const mujeres = prompt('Ingresar cantidad de alumnas: ');
const hombres = prompt('Ingresar cantidad de alumnos: ');
const totalAlumnos = parseInt(hombres) + parseInt(mujeres);

function promedioAlumnos(alumnos) {
    return (alumnos * 100) / totalAlumnos
};

const promedioMujeres = promedioAlumnos(mujeres).toFixed(0);
const promedioHombres = promedioAlumnos(hombres).toFixed(0);

console.log('El promedio de mujeres es de ' + promedioMujeres + '\nEl promedio de hombres es ' + promedioHombres + '\nEl total de alumnos es ' + totalAlumnos);





function funcionCallback (elemento, indice, array) {
    console.log("lista[" + indice + "] = " + elemento);
}

var lista = [2, 5, 9];

lista.forEach(funcionCallback);

// Imprime en la consola:
// lista[0] = 2
// lista[1] = 5
// lista[2] = 9