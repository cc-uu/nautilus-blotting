<?php

/**
 * Ruta del home
 */


 Route::get('/', 'HomeController@index');
 Route::get('/empresa', 'QuienesSomosController@index');
 Route::get('/productos', 'ServiciosController@index');
 Route::get('/novedades', 'NovedadesController@index');
 Route::get('/contacto', 'ContactoController@index');

 Route::get('/novedades/{id}/{slug}', 'NovedadesController@novedadAmpliada');
 Route::get('/get-contenidos-by-tag/{xIdContenido}/{xTag}', 'MainController@getContenidosByTag');


/**
 * Rutas de Errores
 */
Route::get('/error', 'ErrorController@error');
Route::get('/error/401', 'ErrorController@error401')->name('error401');
Route::get('/error/403', 'ErrorController@error403')->name('error403');
Route::get('/error/404', 'ErrorController@error404')->name('error404');
Route::get('/error/419', 'ErrorController@error419')->name('error419');
Route::get('/error/429', 'ErrorController@error429')->name('error429');
Route::get('/error/500', 'ErrorController@error500')->name('error500');
Route::get('/error/503', 'ErrorController@error503')->name('error503');

Route::get('/paginaerror', 'PaginaerrorController@index');

/**
 * Rutas de Catalogo
 */

/**
* Rutas del carrito
*/

/**
* csrf Token
*/
Route::get('refresh-csrf', function () {
    return csrf_token();
});
