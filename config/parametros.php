<?php
return [

  #Banner Lotes
  'idBannerL' => 11957,
  'idBannerLCont' => 59934,

  #Redes Sociales
  'idSociales' => 11952,
  'offsetSociales' => 0,
  'limitSociales' => 999,
  'orderBySociales' => 'orden',
  'orderTypeSociales' => 'ASC',


  #Galeria de fotos
  'idGaleria' => 11954,
  'idGaleriaCont' => 57654,

	# Colores
  'idColores' => 11946,
	'idColoresCont' => 59903,

  #Icono
  'idIcono' => 11946,
  'idIconoCont' => 59904,

	# General
	'idNConfiguracion' => 11946,
	'idNConfiguracionCont' => 59900,
	'offsetNConfiguracion' => 0,
	'limitNConfiguracion' => 999,
	'orderByNConfiguracion' => 'orden',
	'orderTypeNConfiguracion' => 'ASC',

	# Home Titulo
	'idNConfiguracionTit' => 11946,
	'idNConfiguracionTitCont' => 59895,

	#Error
	'idError'=> 11946,
	'idErrorCont'=> 59902,

	# General Home
	'idNConfiguracionHome' => 11947,
	'idNConfiguracionHomeCont' => 59905,
	'idHome' => 52548,
	'cantPalabrasNovedades' => 20,
	'finPalabrasNovedades' => '...',

	'obtenerTags' => [
	'Novedades2',
	],

    # Logos
	'idConfigLogo' => 11946,
	'idConfigLogoCont' => 59900,
	'offsetLog' => 0,
	'limitLog' => 999,
	'orderByLog' => 'orden',
	'orderTypeLog' => 'ASC',

	# Meta
	'idMeta' => 11946,
	'idMetaCont' => 59901,

	# Slide
	'idSlide' => 11948,
  'offsetSlide' => 0,
	'limitSlide' => 6,
	'orderBySlide' => 'orden',
  'orderTypeSlide' => 'ASC',

	# Video
	'idVideo' => 11951,
	'offsetVideo' => 0,
	'limitVideo' => 6,
	'orderByVideo' => 'orden',
  'orderTypeVideo' => 'ASC',

	# Servicios
	'idServicios' => 11956,
	'offsetServicios' => 0,
	'limitServicios' => 99,
	'orderByServicios' => 'orden',
	'orderTypeServicios' => 'ASC',
  # 'cantPalabrasServicios' => 100,
	# 'finPalabrasServicios' => '...',

	# Footer
	'idFooter' => 11952,
	'idFooterCont' => 59915,

  	# Novedades
	'idNovedades' => 11959,
 	'offsetNovedades' => 0,
 	'limitNovedades' => 9999,
 	'orderByNovedades' => 'orden',
 	'orderTypeNovedades' => 'ASC',
	'porPaginaNovedades' => 12,

	# Novedades 1
	'idNovedades1' => 11959,
	'offsetNovedades1' => 0,
	'limitNovedades1' => 999,
	'orderByNovedades1' => 'orden',
	'orderTypeNovedades1' => 'ASC',

	# Novedades 2
	'idNovedades2' => 11959,
	'offsetNovedades2' => 0,
	'limitNovedades2' => 2,
	'orderByNovedades2' => 'orden',
	'orderTypeNovedades2' => 'ASC',

	# Banner
	'idBanner' => 11949,
	'offsetBanner' => 0,
	'limitBanner' => 1,
	'orderByBanner' => 'orden',
	'orderTypeBanner' => 'ASC',

	# Logos
	'idLogos' => 11950,
	'offsetLogos' => 0,
	'limitLogos' => 999,
	'orderByLogos' => 'orden',
	'orderTypeLogos' => 'ASC',

	# Logos Titulo
  'idLogosTitulo' => 11950,
	'idLogosTituloCont' => 59910,

	# Pàginas
	'idQuienesSomos'=>11953,
	'idQuienesSomosCont' =>59919,
	'idModulo' => 11954,
	'offsetModulo'=> 0,
	'limitModulo' => 999,
	'orderByModulo' => 'orden',
	'orderTypeModulo' => 'ASC',
	'idContacto' => 11960,
	'idContactoCont' => 59949,
	'idIntroServi' => 11955,
	'idIntroServiCont' => 59923,
	'idIntroNovedades' => 11958,
	'idIntroNovedadesCont' => 59943,
  'idModulosServicios' => 11957,
	'offsetModulosServicios'=> 0,
	'limitModulosServicios' => 999,
	'orderByModulosServicios' => 'orden',
	'orderTypeModulosServicios' => 'ASC',
];
