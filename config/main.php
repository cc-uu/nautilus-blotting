<?php
return [
	'ID_SITIO' => '235',
	'URL_API' => 'https://apirest.fidelitytools.net/api/',
	'URL_REST' => 'https://rest.fidelitytools.net/Fidelitytools/',
	'URL_WEBAPI' => 'https://rest.fidelitytools.net',
	'URL_WS' => 'https://ws.fidelitytools.net/v2/api',
	'FIDELITY_KEY' => '265c092ff1813072ffeb07ec2ab84e4d',
	'WS_KEY' => '5d52c64c5cb8340f980bdaab',

	'PAGE_CACHE' => false,
	'ID_ANALYTIICS' => '123',
	'IDIOMA_DEFAULT' => 'es',
	'MULTIDIOMAS' => true,
	'NOMBRE_SITIO' => "Nautilus | Proyectos Digitales Corporativos",

	'ID_DESCUENTOS' => 0,
	'ID_ECOMMERCE' => 3565,
	'ID_ECOMMERCE_ENCODE' => 'MzU2NQ',

	'JS_MAP' => false,
	'JS_SCROLLING' => false,
	'JS_SCROLLING_LIMIT' => false,
	'JS_SCROLLING_LOADING' => true,
	'JS_SHOPPING_CART' => false,

	'META_DESCRIPTION' => 'Tu sitio web corporativo 100% autogestionable con Nautilus, una propuesta rápida para iniciar tu negocio o dar a conocer tu servicio con una perspectiva 360. Córdoba, Argentina.',
	'META_KEYWORDS' => 'Nautilus, Páginas web, Plantillas, Diseño responsive, sitio web, Maquetado, html5, CSS3, framework, Bootstrap 4.5, programado, PHP 7.3, Laravel 5.8, Base de datos, Fidelitytools, ASPX:NET, MySQL, secciones, home, quienes somos, servicios, novedades, contacto, logo, personalizar, personalización, combinar, combinación, colores, Estructura, preestablecida, preestablecidos, módulo, módulos, contenido, contenidos, autogestionable, autogestionables, Google Maps',

	'USUARIO_TWITTER' => '@',
	'IS_CACHE' => (isset($_SERVER['SERVER_ADDR']) AND ($_SERVER['SERVER_ADDR'] == '10.1.2.10')) ? true : false,
	'CACHE' => false,
	'CACHE_CATEGORIA' => 7200,
	'CACHE_CONTENIDO' => 5,
	'CACHE_PERSONA' => 3600,
	'CACHE_PRODUCTO' => 600,
	'CACHE_ARCHIVO' => 7200,
	'CACHE_ETIQUETA' => 7200,
];
