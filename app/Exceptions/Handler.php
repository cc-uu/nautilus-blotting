<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if($this->isHttpException($e))
        {
            switch (intval($e->getStatusCode())) {
                // not found
                case 401:
                    return redirect()->route('error401');
                    break;
                case 403:
                    return redirect()->route('error403');
                    break;
                 case 404:
                    return redirect()->route('error404');
                    break;
                 case 419:
                    return redirect()->route('error419');
                    break;
                case 429:
                    return redirect()->route('error429');
                    break;
                case 500:
                     return redirect()->route('error500');
                    break;
                case 503:
                    return redirect()->route('error503');
                    break;
                default:
                    return $this->renderHttpException($e);
                    break;
            }
        }
        else
        {
            return parent::render($request, $e);
        }
    }
}
