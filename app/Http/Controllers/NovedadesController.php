<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class NovedadesController extends MainController
{
    public function index(){
        $oContenido = new \App\Clases\Contenido\Contenido();
        $xConfiguracion = $this->obtenerConfiguracion();
        $xLogo = $xConfiguracion['xLogo'];
        $xMenu = $xConfiguracion['xMenu'];
        $xFooter = $xConfiguracion['xFooter'];
        $aSociales = $xConfiguracion['aSociales'];
        $oTag = new \App\Clases\Contenido\Tag();
        $xIntroNovedades = $oContenido->getById(config('parametros.idIntroNovedades'), config('parametros.idIntroNovedadesCont'));

        isset($_GET['pagina']) ? $xPagina = $_GET['pagina'] : $xPagina = '1';
        $xOffset = ($xPagina - 1) * config('parametros.porPaginaNovedades');

        $xTotalNovedades = $oContenido->getAll(
            config('parametros.idNovedades'),
            config('parametros.offsetNovedades'),
            config('parametros.limitNovedades'),
            config('parametros.orderByNovedades'),
            config('parametros.orderTypeNovedades')
        );


        if(!empty($xTotalNovedades))
            $xTotalPaginas = ceil(count($xTotalNovedades) / config('parametros.porPaginaNovedades'));
        else
            $xTotalPaginas = 0;

        $xNovedades = $oContenido->getAll(
            config('parametros.idNovedades'),
            $xOffset,
            config('parametros.porPaginaNovedades'),
            config('parametros.orderByNovedades'),
            config('parametros.orderTypeNovedades')
        );

        return view('novedades.index', compact('xLogo', 'xMenu', 'xIntroNovedades', 'xNovedades', 'xFooter', 'xPagina', 'xTotalPaginas', 'xTotalNovedades', 'aSociales'));
    }

    public function novedadAmpliada($id, $nombre){

        $oContenido = new \App\Clases\Contenido\Contenido();
        $xConfiguracion = $this->obtenerConfiguracion();
        $xLogo = $xConfiguracion['xLogo'];
        $xMenu = $xConfiguracion['xMenu'];
        $xFooter = $xConfiguracion['xFooter'];
        $aSociales = $xConfiguracion['aSociales'];
        $xNovedad = $oContenido->getById(config('parametros.idNovedades'), $id);


        return view ('novedades.novedad-ampliada', compact('xLogo', 'xMenu', 'xFooter', 'xNovedad', 'aSociales'));
    }

}
