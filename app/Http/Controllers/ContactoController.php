<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ContactoController extends MainController
{
    public function index() {
        $oContenido = new \App\Clases\Contenido\Contenido();
        $xConfiguracion = $this->obtenerConfiguracion();
        $xLogo = $xConfiguracion['xLogo'];
        $xMenu = $xConfiguracion['xMenu'];
        $xFooter = $xConfiguracion['xFooter'];
        $aSociales = $xConfiguracion['aSociales'];

        $xContacto = $oContenido->getById(
            config('parametros.idContacto'),
            config('parametros.idContactoCont')
        );
      

       if (!empty($xContacto)){

       }
        return view('contacto.index', compact('xLogo', 'xMenu', 'xContacto', 'xFooter', 'aSociales'));

    }
}
