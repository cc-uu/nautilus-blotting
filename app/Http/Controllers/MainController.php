<?php
namespace App\Http\Controllers;

use Illuminate\Support\Str;

class MainController extends Controller
{
    public function obtenerConfiguracion()
    {
        $oContenido = new \App\Clases\Contenido\Contenido();
        $xLogo = $oContenido->getById(config('parametros.idConfigLogo'), config('parametros.idConfigLogoCont'));
        $xFooter = $oContenido->getById(config('parametros.idFooter'), config('parametros.idFooterCont'));
        $xMeta = $oContenido->getById(config('parametros.idMeta'), config('parametros.idMetaCont'));

        $xSociales = $oContenido->getAll(
    			config('parametros.idSociales'),
    			config('parametros.offsetSociales'),
    			config('parametros.limitSociales'),
    			config('parametros.orderBySociales'),
    			config('parametros.orderTypeSociales')
    	);

    	$aSociales = [];

    		if(!empty($xSociales)){
    			foreach ($xSociales as $val){
    				if ($val->tags == 'redes'){
    					$aSociales[] = [
    						 'idContenido' => $val->idContenido,
    						 'titulo' => $val->titulo,
    						 'tags' => $val->tags,
    						 'bajada' => $val->bajada,
    						 'imagenes' => $val->imagenes,
    						 'videos' => $val->videos,
    					];
    				}
    			}
    		}


        $xHome = $oContenido->getAll(
            config('parametros.idNConfiguracion'),
            config('parametros.offsetNConfiguracion'),
            config('parametros.limitNConfiguracion'),
            config('parametros.orderByNConfiguracion'),
            config('parametros.orderTypeNConfiguracion')
        );


        $xMenu= [];
        foreach ($xHome as $key => $val) {
            if ($val->tags == 'menu') {
                $xMenu[]=[
                     'titulo' => $val->titulo,
                     'enlace' => '/'.Str::slug($val->bajada)
                 ];
            }
        }

        return [
            'xLogo' => $xLogo,
            'xMenu' => $xMenu,
            'xFooter' => $xFooter,
            'xMeta' => $xMeta,
            'aSociales' => $aSociales,
        ];

    }

    public function getContenidosByTag($xIdContenido, $xTag)
    {
        try {
            $oTag = new \App\Clases\Contenido\Tag();
            $xServicios = $oTag->getContenidos($xIdContenido, $xTag, 0, 100);
            $aServicios = [];
            foreach ($xServicios as $val) {
                $aServicios[] = [
                    'idContenido' => $val->idContenido,
                    'titulo' => $val->titulo,
                    'slug' => Str::slug($val->titulo),
                    'descripcion' => Str::words(strip_tags($val->descripcion)), #config('parametros.cantPalabrasNovedades'), config('parametros.finPalabrasNovedades')),
                    'imagenes' => $val->imagenes,
                    'detalle' => $val->detalle,
                ];
            }

            return $aServicios;
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'data' => $e->getMessage()]);
        }
    }
}
