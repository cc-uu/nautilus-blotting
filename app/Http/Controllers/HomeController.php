<?php
namespace App\Http\Controllers;

/**
 *  [index description]  -> Muestra la aplicacion.
 *  [prueba description]  -> Funcion para realizar pruebas unicamente.
 */

class HomeController extends MainController {
	public function index() {
		$oContenido = new \App\Clases\Contenido\Contenido();
		$oTag = new \App\Clases\Contenido\Tag();
		$xConfiguracion = $this->obtenerConfiguracion();
		$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xConfigHome = $oContenido->getById(config('parametros.idNConfiguracionHome'), config('parametros.idNConfiguracionHomeCont'));
		$xLogostitulos = $oContenido->getById(config('parametros.idLogosTitulo'), config('parametros.idLogosTituloCont'));
		$aSociales = $xConfiguracion['aSociales'];



		$xParametros = [];
		$xIndex = -1;

		$aConfigHome = [
			  'titulo' => ucfirst($xConfigHome->titulo),
			  'bajada' => ucfirst($xConfigHome->bajada),
			  'subtitulo' => ucfirst($xConfigHome->subtitulo),
			  'precio' => ucfirst($xConfigHome->precio),
			  'link' => ucfirst($xConfigHome->link),
			  'fuente' => ucfirst($xConfigHome->fuente),
			  'tags' => ucfirst($xConfigHome->tags),
		 ];

		foreach($aConfigHome as $val){
			$xIndex += 1;
			if(!empty($val)){
				$xContenido = $oContenido->getAll(
					config('parametros.id'.$val),
					config('parametros.offset'.$val),
					config('parametros.limit'.$val),
					config('parametros.orderBy'.$val),
					config('parametros.orderType'.$val)
				);


				if (!empty($xContenido)){
					$xParametros[$xIndex] = [
						'contenido' => $xContenido,
						'tipo' => $val
					];
					if(in_array($val, config('parametros.obtenerTags'))){
						$xParametros[$xIndex]['tags'] = $oTag->getAll(config('parametros.id'.$val));
					}
				}

			}
		}



		return view('home.index', compact('xLogo', 'xMenu', 'xParametros', 'xFooter', 'xMeta', 'xLogostitulos', 'aSociales'));

	}

}
