<?php
namespace App\Http\Controllers;

/**
 *  [index description]  -> Muestra la aplicacion.
 *  [prueba description]  -> Funcion para realizar pruebas unicamente.
 */

class ErrorController extends MainController {
	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function error401() {
		$xConfiguracion = $this->obtenerConfiguracion();
		$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xError = $oContenido->getById(config('parametros.idError'), config('parametros.idErrorCont'));
		$aSociales = $xConfiguracion['aSociales'];

		return view('layouts.Paginaerror', compact('xLogo', 'xMenu', 'xFooter', 'xMeta', 'xError', 'aSociales'));
	}

	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function error403() {
		$xConfiguracion = $this->obtenerConfiguracion();
		$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xError = $oContenido->getById(config('parametros.idError'), config('parametros.idErrorCont'));
		$aSociales = $xConfiguracion['aSociales'];

		return view('layouts.Paginaerror', compact('xLogo', 'xMenu', 'xFooter', 'xMeta', 'xError', 'aSociales'));
	}

	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function error404() {
		$oContenido = new \App\Clases\Contenido\Contenido();
		$xConfiguracion = $this->obtenerConfiguracion();
     	$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xError = $oContenido->getById(config('parametros.idError'), config('parametros.idErrorCont'));
		$aSociales = $xConfiguracion['aSociales'];

		return view('layouts.Paginaerror', compact('xLogo', 'xMenu', 'xFooter', 'xMeta', 'xError', 'aSociales'));

	}

	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function error419() {
		$xConfiguracion = $this->obtenerConfiguracion();
		$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xError = $oContenido->getById(config('parametros.idError'), config('parametros.idErrorCont'));
		$aSociales = $xConfiguracion['aSociales'];

		return view('layouts.Paginaerror', compact('xLogo', 'xMenu', 'xFooter', 'xMeta', 'xError', 'aSociales'));
	}

	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function error429() {
		$xConfiguracion = $this->obtenerConfiguracion();
		$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xError = $oContenido->getById(config('parametros.idError'), config('parametros.idErrorCont'));
		$aSociales = $xConfiguracion['aSociales'];

		return view('layouts.Paginaerror', compact('xLogo', 'xMenu', 'xFooter', 'xMeta', 'xError', 'aSociales'));
	}

	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function error500() {
		$xConfiguracion = $this->obtenerConfiguracion();
		$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xError = $oContenido->getById(config('parametros.idError'), config('parametros.idErrorCont'));
		$aSociales = $xConfiguracion['aSociales'];

		return view('layouts.Paginaerror', compact('xLogo', 'xMenu', 'xFooter', 'xMeta', 'xError', 'aSociales'));
	}

	/**
	 * Muestra la aplicacion dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function error503() {
		$xConfiguracion = $this->obtenerConfiguracion();
		$xLogo = $xConfiguracion['xLogo'];
		$xMenu = $xConfiguracion['xMenu'];
		$xFooter = $xConfiguracion['xFooter'];
		$xMeta = $xConfiguracion['xMeta'];
		$xError = $oContenido->getById(config('parametros.idError'), config('parametros.idErrorCont'));
		$aSociales = $xConfiguracion['aSociales'];

		return view('layouts.Paginaerror', compact('xLogo', 'xMenu', 'xFooter', 'xMeta', 'xError', 'aSociales'));
	}
}
