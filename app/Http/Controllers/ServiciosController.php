<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ServiciosController extends MainController
{
    public function index() {
        $oContenido = new \App\Clases\Contenido\Contenido();
        $xConfiguracion = $this->obtenerConfiguracion();
        $xLogo = $xConfiguracion['xLogo'];
        $xMenu = $xConfiguracion['xMenu'];
        $xFooter = $xConfiguracion['xFooter'];
        $oTag = new \App\Clases\Contenido\Tag();
        $xIntroServi = $oContenido->getById(config('parametros.idIntroServi'), config('parametros.idIntroServiCont'));
        $xImgBanner = $oContenido->getById(config('parametros.idBannerL'), config('parametros.idBannerLCont'));
        $aSociales = $xConfiguracion['aSociales'];


          $xTags = $oTag->getAll(
            config('parametros.idServicios'),
            config('parametros.offsetServicios'),
            config('parametros.limitServicios'),
            config('parametros.orderByServicios'),
            config('parametros.orderTypeServicios')
            );
            // config('parametros.cantPalabrasServicios'),
            // config('parametros.finPalabrasServicios')


  


        $xModulosServicios = $oContenido->getAll(
            config('parametros.idModulosServicios'),
            config('parametros.offsetModulosServicios'),
            config('parametros.limitModulosServicios'),
            config('parametros.orderByModulosServicios'),
            config('parametros.orderTypeModulosServicios')
        );

        $aVision = [];
        $aBanner = [];
        $aVideos = [];

        if(!empty($xModulosServicios)){
          foreach ($xModulosServicios as $val){
              if ($val->imagenes[0]->nombre == "default_contenido" || ($val->videos[0]->idVideo == null && $val->videos[1]->idVideo == ""))  {
                  if ($val->tags != 'banner' &&  $val->tags !='video'){
                  $aVision[] = [
                     'idContenido' => $val->idContenido,
                     'titulo' => $val->titulo,
                     'descripcion' => $val->descripcion,
                     'detalle' => $val->detalle
                  ];
                }
             }

              if ($val->tags == 'banner'){
                  $aBanner[] = [

                      'idContenido' => $val->idContenido,
                      'titulo' => $val->titulo,
                      'bajada' => $val->bajada,
                      'descripcion' => $val->descripcion,
                      'link' => $val->link,
                      'subtitulo' => $val->subtitulo,
                      'detalle' => $val->detalle
                  ];
              }

              if ($val->tags == 'video'){
                  $aVideos[] = [

                      'idContenido' => $val->idContenido,
                      'titulo' => $val->titulo,
                      'link' => $val->link,
                      'videos' => $val->videos,
                      'detalle' => $val->detalle
                  ];
              }

            }
        }

        return view('servicios.index', compact('xLogo', 'xMenu', 'xTags', 'xIntroServi', 'xModulosServicios', 'xFooter', 'aVision', 'aBanner', 'aVideos', 'xImgBanner', 'aSociales'));
    }
  }
