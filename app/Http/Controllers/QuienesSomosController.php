<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Http\Request;




class QuienesSomosController extends MainController{

    public function index() {
        $oContenido = new \App\Clases\Contenido\Contenido();
        $xConfiguracion = $this->obtenerConfiguracion();
        $xLogo = $xConfiguracion['xLogo'];
        $xMenu = $xConfiguracion['xMenu'];
        $xFooter = $xConfiguracion['xFooter'];
        $oTag = new \App\Clases\Contenido\Tag();
        $xQuienesSomos = $oContenido->getById(config('parametros.idQuienesSomos'), config('parametros.idQuienesSomosCont'));
        $xGaleria = $oContenido->getById(config('parametros.idGaleria'), config('parametros.idGaleriaCont'));
        $aSociales = $xConfiguracion['aSociales'];



        $xModulo = $oContenido->getAll(
            config('parametros.idModulo'),
            config('parametros.offsetModulo'),
            config('parametros.limitModulo'),
            config('parametros.orderByModulo'),
            config('parametros.orderTypeModulo')
        );



        $aModulo = [];
        $aVision = [];
        $aGaleria = [];


        if(!empty($xModulo)){
            foreach ($xModulo as $val){
                if (count($val->imagenes) == 1 && $val->imagenes[0]->nombre != "default_contenido" || ($val->videos[0]->idVideo != null && $val->videos[1]->idVideo != "")){
                    $aModulo[] = [
                       'idContenido' => $val->idContenido,
                       'titulo' => $val->titulo,
                       'descripcion' => $val->descripcion,
                       'imagenes' => $val->imagenes,
                       'videos' => $val->videos
                    ];
                }
                if(count($val->imagenes) == 1 && $val->imagenes[0]->nombre == "default_contenido"){
                    $aVision[] = [
                       'idContenido' => $val->idContenido,
                       'titulo' => $val->titulo,
                       'descripcion' => $val->descripcion,
                       'imagenes' => $val->imagenes,
                       'videos' => $val->videos
                    ];
                }
                if(count($val->imagenes) > 1 ){
                    $aGaleria[] = [
                        'idContenido' => $val->idContenido,
                        'titulo' => $val->titulo,
                        'descrpicion' => $val->descripcion,
                        'imagenes' => $val->imagenes,
                        'videos' => $val->videos
                    ];
            }
        }



        return view('quienes-somos.index', compact('xLogo', 'xMenu', 'xQuienesSomos', 'xFooter', 'aModulo', 'aVision', 'aGaleria', 'aSociales'));

	}
 }
}
