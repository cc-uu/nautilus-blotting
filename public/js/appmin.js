
/* Menu collapse versión mobile */
$('.navbar-toggler').on('click', function() {
    $(this).toggleClass('active');
    $('#menu-md').animate({height: 'toggle'}, 200);
});


/* Animación del icono del menu mobile */
var hamburguesa = $(".hamburguesa");
var linea1 = $("#linea1");
var linea2 = $("#linea2");
var linea3 = $("#linea3");

hamburguesa.click(function(){
  linea1.toggleClass("rotar1");
  linea2.toggleClass("desaparecer");
  linea3.toggleClass("rotar2");
});

async function getContenidos(xTag, xIdContenido) {
    let url = Sawubona.baseUrl+'/get-contenidos-by-tag/'+encodeURIComponent(xIdContenido)+'/'+encodeURIComponent(xTag)
    console.log(url)
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}

async function renderServices(xTag, xIdContenido) {

    let services = await getContenidos(xTag, xIdContenido);

    let html = '';
    services.forEach(service => {
        console.log(service);
        let htmlSegment =
        `<div class="row contenedor-servicios">
            <div class="col-12 col-sm-6">`;

        if(service.imagenes.length > 1){
          htmlSegment +=
          `<div id="carouselServicios" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">`;

                service.imagenes.forEach((servic, index) => {
                 htmlSegment +=
                  `<div class="carousel-item ${index==0 ? 'active' : ''}">
                    <img class="d-block w-100" src="${servic.path}" alt="First slide">
                  </div>`;
                });

              htmlSegment += `
              </div>
              <a class="carousel-control-prev" href="#carouselServicios" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Anterior</span>
              </a>
              <a class="carousel-control-next" href="#carouselServicios" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Siguiente</span>
              </a>
          </div>`;
        }else{
          htmlSegment +=
          `<div class="centrar-img">
              <img src="${service.imagenes[0].path}" alt="">
          </div>`;

        }

        htmlSegment +=
        `</div>
        <div class="col-xl-5 offset-xl-1 col-lg-6 col-md-6 col-sm-6">
        <h1>${service.titulo}</h1>
        <p>${service.detalle}</p>
        </div>
        </div>`;

        html += htmlSegment;
    });

    let container = document.querySelector('#resultados-servicios');
    container.innerHTML = html;
}

async function renderNews(xTag, xIdContenido) {
    let news = await getContenidos(xTag, xIdContenido);
    let html = '';
    news.forEach(n => {
        let htmlSegment =

        `<div class="col-md-6">
            <div class="modulo-galeria">
                <div class="centrar-img">
                    <a href="${Sawubona.baseUrl}/comunidad/${n.idContenido}/${n.slug}">
                        <img src="${n.imagenes[0].path}" alt="${n.titulo}">
                    </a>
                </div>
                <a href="${Sawubona.baseUrl}/comunidad/${n.idContenido}/${n.slug}">
                    <h1>${n.titulo}</h1>
                </a>
                <p class="d-none d-md-block">${n.descripcion}</p>
            </div>
        </div>`;

        html += htmlSegment;
    });

    let container = document.querySelector('#resultados-novedades');
    container.innerHTML = html;
}

$('#buscador-servicios').on('change', function(){
  console.log("buscador")
    let xTag = $(this).val().split('*')[0];
    let xIdContenido = $(this).val().split('*')[1];
    renderServices(xTag, xIdContenido)
})

$('#buscador-novedades').on('change', function(){

    if($(this).val()=="todas")
        window.location.href=Sawubona.baseUrl+'/novedades'

    let xTag = $(this).val().split('*')[0];
    let xIdContenido = $(this).val().split('*')[1];
    renderNews(xTag, xIdContenido)
})

$( document ).ready(function() {
  console.log("buscador")
  let xTag = $('#buscador-servicios').val().split('*')[0];
  let xIdContenido = $('#buscador-servicios').val().split('*')[1];
  renderServices(xTag, xIdContenido)
});
